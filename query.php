<?php
$location = htmlspecialchars($_GET["location"]);
$radius = htmlspecialchars($_GET["radius"]);
$curl = curl_init();

curl_setopt_array($curl, array(
  CURLOPT_URL => "https://maps.googleapis.com/maps/api/place/nearbysearch/json?&fields=name&location=".$location."&radius=".$radius."&type=shopping_mall"."&key=AIzaSyDiQ-w60bX1L5rYeM-vnnOQtGeP3Vn4Raw",
  CURLOPT_RETURNTRANSFER => true,
  CURLOPT_ENCODING => "",
  CURLOPT_MAXREDIRS => 10,
  CURLOPT_TIMEOUT => 30,
  CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
  CURLOPT_CUSTOMREQUEST => "GET",
  CURLOPT_HTTPHEADER => array(
    "cache-control: no-cache",
    "postman-token: cb3f5223-901c-535a-4454-6fea46c7f7fe"
  ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
  echo "cURL Error #:" . $err;
} else {
  echo $response;
}